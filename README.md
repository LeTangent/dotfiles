# Dotfiles

Ce répertoire accueille mes "dotfiles" personnels.
Il est destiné à permettre une sauvegarde et une (ré)installation rapide de ces fichiers sur mes configurations.
