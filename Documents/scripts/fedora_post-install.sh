#!/bin/bash
#
# fedora_post-install.sh

# script à exécuter en tant que $USER dans une console virtuelle !!!
# exemple :
# - se déconnecter de GNOME,
# - puis Ctrl + Alt + F2
# - se logger
# - aller dans le répertoire contenant le script (cd trucmuche)
# - sh ./fedora_post-install.sh

##################################################
#
# Préalables :
#
# 1°) Fedora GNOME
# 2°) BTRFS
# 3°) Système mis à jour après installation
# 4°) Aucune autre modification effectuée
# 5°) Minimum 8 Go de RAM
# 6°) Système connecté à internet
#
#################################################

# vérification des mises à jour :
sudo dnf update -y

### DNF #########################################
#################################################

# Configuration du gestionnaire de paquets DNF :
# sélectionner le miroir le plus rapide,
# configurer les téléchargements en parallèle,
# refuser la télémétrie,
# préférer les mises à jour via les "deltas".

printf "%s" "
fastestmirror=true
max_parallel_downloads=6
countme=false
deltarpm=true
" | sudo tee -a /etc/dnf/dnf.conf

### RPM Fusion #################################
################################################

sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-"$(rpm -E %fedora)".noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-"$(rpm -E %fedora)".noarch.rpm
sudo dnf groupupdate core -y

# Passer à un ffmpeg complet :
sudo dnf swap ffmpeg-free ffmpeg --allowerasing -y

# codecs multimédia additionnels
sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin -y
sudo dnf groupupdate sound-and-video -y

# GPU Intel ancien :
sudo dnf install -y libva-intel-driver

# gestion de la lecture de DVD :
sudo dnf install -y rpmfusion-free-release-tainted
sudo dnf install -y libdvdcss

# répertoire de firmwares divers 
# (l'installation de firmwares additionnels est commentée : je n'en ai pas l'utilité)
sudo dnf install -y rpmfusion-nonfree-release-tainted
# sudo dnf --repo=rpmfusion-nonfree-tainted install -y "*-firmware"

# codecs complémentaires :
sudo dnf install -y gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugins-bad-free-extras gstreamer1-vaapi gstreamer1-plugin-openh264 --exclude=gstreamer1-plugins-bad-free-devel
sudo dnf install -y lame\* --exclude=lame-devel
sudo dnf group upgrade --with-optional Multimedia -y

### NETTOYAGE (1) ################################
##################################################

sudo dnf remove -y anaconda* anthy-unicode bluez-cups fedora-chromium-config mediawriter ModemManager orca ppp samba-client trousers xorg-x11-drv-vmware
sudo dnf remove -y eog cheese gnome-classic-session gnome-tour gnome-boxes rhythmbox gnome-maps gnome-terminal
sudo dnf autoremove -y

### RPMS ########################################
#################################################

sudo dnf install -y optipng deja-dup aspell-fr unace unrar p7zip flac vorbis-tools opus-tools udftools zstd
sudo dnf install -y gnome-console gnome-tweaks geary brasero meld dconf-editor inxi ncompress file-roller
sudo dnf install -y figlet lolcat neofetch htop soundconverter drawing
# uniquement pour les pc portables :
# sudo dnf install -y tlp    
# sudo systemctl enable tlp

# polices :
sudo dnf install -y fira-code-fonts 'mozilla-fira*' 'google-roboto*'

# polices Microsoft :
sudo dnf install -y curl cabextract xorg-x11-font-utils fontconfig
sudo rpm -i https://downloads.sourceforge.net/project/mscorefonts2/rpms/msttcore-fonts-installer-2.6-1.noarch.rpm

# émojis :
sudo dnf install -y eosrei-emojione-fonts

### FLAPAKS ######################################
##################################################

sudo flatpak update -y

# GNOME Apps
sudo flatpak install flathub -y org.gnome.Snapshot org.gnome.Maps org.gnome.Extensions org.gnome.Music
sudo flatpak install flathub -y org.gnome.Boxes org.gnome.Loupe org.gnome.Epiphany org.gnome.Polari

# GNOME Circle
sudo flatpak install flathub -y com.rafaelmardojai.Blanket com.github.ADBeveridge.Raider com.belmoussaoui.Obfuscate 
sudo flatpak install flathub -y com.github.huluti.Curtail org.gnome.gitlab.YaLTeR.VideoTrimmer
sudo flatpak install flathub -y de.haeckerfelix.Fragments re.sonny.Junction fr.romainvigier.MetadataCleaner 
sudo flatpak install flathub -y org.gnome.Podcasts de.haeckerfelix.Shortwave app.drey.Warp

# Autres applications
sudo flatpak install flathub -y com.spotify.Client io.freetubeapp.FreeTube io.github.mpobaschnig.Vaults
sudo flatpak install flathub -y com.github.tchx84.Flatseal xyz.ketok.Speedtest io.github.seadve.Kooha
sudo flatpak install flathub -y flathub com.simplenote.Simplenote org.pitivi.Pitivi fr.handbrake.ghb
sudo flatpak install flathub -y fi.skyjake.Lagrange

### YT-DLP ########################################
###################################################

cd ~ /Téléchargements
mkdir ~/.local/bin
wget https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp
mv yt-dlp ~/.local/bin
chmod +x ~/.local/bin/yt-dlp
cd

### GNOME #########################################
###################################################

# indicateurs dans le panneau du shell GNOME
sudo dnf install -y gnome-extensions-app
sudo dnf install -y gnome-shell-extension-appindicator

# configuration à la "Pop" (System 76) :
# 1°) le tiling :
# sudo dnf install -y gnome-shell-extension-pop-shell
# 2°) les raccourcis clavier spécifiques :
# sudo dnf install -y make cargo rust gtk3-devel
# git clone https://github.com/pop-os/shell-shortcuts /home/$USER/fedora/pop-theme/shell-shortcuts
# cd /home/$USER/fedora/pop-theme/shell-shortcuts
# make
# sudo make install
# pop-shell-shortcuts
# 3°) Installation du thème GTK :
sudo dnf install -y sassc meson glib2-devel
git clone https://github.com/pop-os/gtk-theme /home/$USER/fedora/pop-theme/gtk-theme
cd /home/$USER/fedora/pop-theme/gtk-theme
meson build && cd build
ninja
sudo ninja install
# 4°) Jeu d'icônes :
git clone https://github.com/pop-os/icon-theme /home/$USER/fedora/pop-theme/icon-theme
cd /home/$USER/fedora/pop-theme/icon-theme
meson build
sudo ninja -C "build" install

# Réglages du shell GNOME :
gsettings set org.gnome.desktop.interface gtk-theme "Pop-Dark"
gsettings set org.gnome.desktop.interface icon-theme "Pop"
gsettings set org.gnome.desktop.interface cursor-theme "Pop"

gsettings set org.gnome.desktop.interface clock-format 24h
gsettings set org.gnome.desktop.interface clock-show-date true
gsettings set org.gnome.desktop.interface clock-show-weekday true
gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
gsettings set org.gnome.desktop.interface document-font-name 'Roboto 11'
gsettings set org.gnome.desktop.interface enable-hot-corners false
gsettings set org.gnome.desktop.interface font-name 'Roboto 11'
gsettings set org.gnome.desktop.interface monospace-font-name 'Source Code Pro 10'
# pour les pc portables uniquement :
# gsettings set org.gnome.desktop.interface show-battery-percentage true
gsettings set org.gnome.desktop.notifications show-in-lock-screen false
gsettings set org.gnome.desktop.privacy old-files-age 7
gsettings set org.gnome.desktop.privacy remove-old-trash-files true
gsettings set org.gnome.desktop.privacy report-technical-problems false
gsettings set org.gnome.desktop.sound event-sounds false
gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'
gsettings set org.gnome.desktop.wm.preferences titlebar-font 'Roboto Bold 11'

# GDM
gsettings set org.gnome.login-screen disable-user-list true
gsettings set org.gnome.login-screen enable-fingerprint-authentication false

# GNOME Software :
gsettings set org.gnome.software download-updates false
gsettings set org.gnome.software download-updates-notify false

# Nautilus :
gsettings set org.gnome.desktop.notifications.applications.org-gnome-nautilus enable-sound-alerts false
gsettings set org.gnome.desktop.notifications.applications.org-gnome-nautilus details-in-lock-screen false 
gsettings set org.gnome.desktop.notifications.applications.org-gnome-nautilus show-in-lock-screen false
gsettings set org.gnome.nautilus.preferences click-policy 'single'
gsettings set org.gnome.nautilus.preferences default-folder-viewer icon-view
gsettings set org.gtk.gtk4.settings.file-chooser sort-directories-first true

# Editeur de texte :
gsettings set org.gnome.TextEditor highlight-current-line true
gsettings set org.gnome.TextEditor restore-session false
gsettings set org.gnome.TextEditor show-line-numbers true
gsettings set org.gnome.TextEditor spellcheck false

### FIREFOX #######################################
###################################################

# Autoriser la lecture avec le codec OpenH264
sudo dnf config-manager --set-enabled fedora-cisco-openh264
sudo dnf install -y gstreamer1-plugin-openh264 mozilla-openh264sudo dnf autoremove -y

# Thème GNOME pour Firefox (à appliquer ensuite via les paramètres du navigateur...) :
cd ~/Téléchargements
curl -s -o- https://raw.githubusercontent.com/rafaelmardojai/firefox-gnome-theme/master/scripts/install-by-curl.sh | bash
cd

### TOR ##########################################
##################################################

sudo dnf install -y tor torbrowser-launcher
sudo systemctl enable tor
sudo flatpak install -y flathub org.onionshare.OnionShare

### FSTAB ########################################
##################################################

sudo su -
# options de montage des partitions btrfs
sed -i 's/compress=zstd:1/ssd,noatime,compress=zstd:1/g' /etc/fstab
# tmpfs
echo "tmpfs /tmp tmpfs defaults,mode=1777,nosuid,size=4196M 0 0" >> /etc/fstab
exit

### SECURITE ######################################sudo dnf autoremove -y
###################################################

#  umask à 077
umask 077
sudo sed -i 's/umask 022/umask 077/g' /etc/bashrc

# Rendre le dossier personnel privé
chmod 700 /home/"$(whoami)"

### DIVERS #######################################
##################################################

# activer fstrim
sudo systemctl enable fstrim.timer

### Configuration personalisée :
# configurer le nom d'hôte
sudo hostnamectl set-hostname prodesk-G400-1
# bashrc :
cd ~/Téléchargements
git clone https://gitlab.com/LeTangent/dotfiles
cd dotfiles
cp .bashrc ~/.bashrc
# fonds d'écran (à appliquer ensuite manuellement !) :
cp -R Images/bg ~/Images/
# bin
cp -R .local/bin/* ~/.local/bin/
chmod +x ~/.local/bin/*
cd

### NETTOYAGE (2) ###############################
#################################################

sudo dnf clean all

